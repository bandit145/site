+++
categories = []
date = "2017-06-10T22:46:37-04:00"
tags = []
title = "puppet"

+++
Puppet is an open-source & enterprise software for configuration management in linux.  Puppet is a  IT automation software used to push configuration to its clients (puppet agents). Puppet uses the concept of main (Puppet Server) and client (Puppet Agent) to get instructions from the main server. The puppet agent needs to be installed in every host that you will manage. Puppet its written in ruby and uses manifest for instructions.
